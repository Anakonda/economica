from django.db import models


class Brif(models.Model):
    id = models.AutoField(primary_key=True)
    fullName = models.TextField('fullName')
    phoneNumber = models.TextField('phoneNumber')
    emailAdress = models.TextField('emailAdress')
    companyName = models.TextField('companyName')
    webType = models.TextField('webType')
    goal = models.TextField('goal')
    services = models.TextField('services')
    selling = models.TextField('selling')
    functions = models.TextField('functions')
    logo = models.TextField('logo')
    palate = models.TextField('palate')
    style = models.TextField('style')
    serving = models.TextField('serving')
    budget = models.IntegerField('budget')
    daterange = models.TextField('daterange')


def addToDB(fullName, phoneNumber, emailAdress, companyName, webType, goal, services, selling, functions, logo, palate,
            style, serving, budget, daterange):
    new = Brif()
    new.fullName = fullName
    new.phoneNumber = phoneNumber
    new.emailAdress = emailAdress
    new.companyName = companyName
    new.webType = webType
    new.goal = goal
    new.services = services
    new.selling = selling
    new.functions = functions
    new.logo = logo
    new.palate = palate
    new.style = style
    new.serving = serving
    new.budget = budget
    new.daterange = daterange
    new.save()
