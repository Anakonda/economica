from django.shortcuts import render, HttpResponse
from django.views.decorators.http import require_POST
from .models import addToDB


def index(request):
    return render(request, 'main/index.html')


@require_POST
def submit(request):
    addToDB(request.POST['fullName'], request.POST['phoneNumber'], request.POST['emailAdress'],
            request.POST['companyName'], request.POST['webType'], request.POST['goal'], request.POST['services'],
            request.POST['selling'], request.POST['functions'], request.POST['logo'], request.POST['palate'],
            request.POST['style'], request.POST['serving'], request.POST['budget'], request.POST['daterange'])
    return render(request, 'main/successful.html')
